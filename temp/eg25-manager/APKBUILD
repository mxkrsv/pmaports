# Forked from Alpine to upgrade to current git master, which has
# Ofono compatibility fixes needed for Plasma Mobile
pkgname=eg25-manager
pkgver=0.3.0_git20210812
pkgrel=0
pkgdesc="Daemon for managing the Quectel EG25 modem"
url="https://gitlab.com/mobian1/devices/eg25-manager"
# s390x, mips64 and riscv64 blocked by polkit -> modemmanager
arch="all !s390x !mips64 !riscv64"
license="GPL-3.0-or-later"
makedepends="
	glib-dev
	libgpiod-dev
	libgudev-dev
	libusb-dev
	meson
	modemmanager-dev
	curl-dev
	"
_commit="e6df81778e812ef73aa55156bea5ede2937f9a4c"
source="
	https://gitlab.com/mobian1/devices/eg25-manager/-/archive/$_commit/eg25-manager-$_commit.tar.gz
	eg25-manager.confd
	eg25-manager.initd
	"
options="!check"  # no tests
subpackages="$pkgname-openrc"
builddir="$srcdir/$pkgname-$_commit"

build() {
	abuild-meson . output

	# For some reason, the header files don't get generated first by the meson
	# command below.
	for i in gdbo-manager.h gdbo-modem.h; do
		ninja -C output src/libgdbofono/$i
	done

	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
	install -Dm755 "$srcdir"/eg25-manager.initd "$pkgdir"/etc/init.d/eg25-manager
	install -Dm644 "$srcdir"/eg25-manager.confd "$pkgdir"/etc/conf.d/eg25-manager
}

sha512sums="
72846ea696ee9d7ec433d5dd932881931d9f1fde2a8edc23e7a8f0d9cd74f41df054ffe15a826d48224b5d1f24ce59a607a93b0da62490ffa06dd8beef2b074c  eg25-manager-e6df81778e812ef73aa55156bea5ede2937f9a4c.tar.gz
55936830afad2968a214fb39cfe1a9db50421dc2ff4f67d04f08f6bd2b094c3ab46799cfc7743bbc5032682d98d1216203adf5264353a05134bea58524ac070b  eg25-manager.confd
0dd866ce18bac37c3832a463205402f5b34a520e1a57cc37658fb37e21a173fbba2cfab223111c68af768be1d3feeb23e41dbaf6d8dc14a2b2c0c088cf3df041  eg25-manager.initd
"
