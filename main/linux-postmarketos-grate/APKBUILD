pkgname=linux-postmarketos-grate
pkgver=5.14.0_rc6
pkgrel=1
arch="armv7"
pkgdesc="Linux kernel with experimental patches for Tegra"
url="https://postmarketos.org"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev
	     devicepkg-dev bison flex openssl-dev xz findutils"
options="!strip !check !tracedeps pmb:cross-native
	pmb:kconfigcheck-nftables pmb:kconfigcheck-zram"
license="GPL-2.0-only"

# Source
_flavor=postmarketos-grate
_commit="f6e4a8d26df39804bfbd793ef4871b38d0ebff84"
_carch="arm"
_config="config-$_flavor.$arch"
source="$pkgname-$pkgver-$_commit.tar.gz::https://github.com/grate-driver/linux/archive/$_commit.tar.gz
	$_config"
builddir="$srcdir/linux-$_commit"
_outdir="out"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make O="$_outdir" ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor" "$_outdir"

	make modules_install dtbs_install \
		O="$_outdir" ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

sha512sums="
1166c043e0334169caf21534128974a9aa97df38dbf1f3543919a5847f7726a2351ee7cbf68fc5b41cc09e153740d802382bfd611b1651dbbc21c45092ada9ce  linux-postmarketos-grate-5.14.0_rc6-f6e4a8d26df39804bfbd793ef4871b38d0ebff84.tar.gz
f1dc8a8a84026721d86099d423ab47ca9b43f8db349733b04747fa449b7dd3588e89099d2b2f3cef75db3823c21b85215bffd0ed2d6672a7882281984317dd0b  config-postmarketos-grate.armv7
"
